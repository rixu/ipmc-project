# TP IPMC

This project is a fork of the [original impc-project](https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-project) and contains drivers for the following sensors
to be used on the TP board:
* ADT7420
* LTC2945

along with other customizations to the IPMC code.