/*
	Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

	Description:
	This header file defines the interfaces
	provided by the sensor_LTC2991.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_LTC2991_H__
#define __MASTER_SENSOR_LTC2991_H__

#include <sensor.h>

/* Read-only info structure of an LTC2991 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned char sns; /* Slave I2C channel number of PCA9545A switch and LTC channel number */
} sensor_ltc2991_ro_t;

/* LTC2991 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_ltc2991_methods;

/* Auxiliary macro for defining LTC2991 sensor info */
#define SENSOR_LTC2991(s, a, alert) \
    { \
	SA(sensor_ltc2991_methods, s, alert), \
	sns:  (a) \
    }

#endif /* __MASTER_SENSOR_LTC2991_H__ */
