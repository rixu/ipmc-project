/*
    IPMC sensor driver for the ADT7420 temperature sensor.
    
    Author: Riley Xu
    Created: 2019/06/10
*/

#define NEED_MASTERONLY_I2C         /* Check that the Sensor_i2c bus is implemented */ 

#include <defs.h>
#include <cfgint.h>
#include <debug.h> 

#ifdef CFG_SENSOR_ADT7420 			/* Compile the source only if at least one ADT7420 is implemented by the user */

//#define DEBUG // rixu better way to activate?

#include <hal/i2c.h>				/* I2C functions */
#include <i2c_dev.h>				/* Master Only I2C functions */
#include <hal/system.h>				/* System functions */
	
#include <app.h>					/* App functions */
#include <log.h>					/* Log functions */
#include <sensor.h>					/* Sensors functions */
#include <sensor_discrete.h>		/* Discrete sensor functions */
#include <sensor_threshold.h>		/* Threshold related functions */
#include <sensor_adt7420.h>         /* Sensor related header file */ 

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use ADT7420 sensors.
#endif

// Get the least-significant byte
#define GET_BYTE_0(x) ((x) & 0xff)
// Get bits 8-15
#define GET_BYTE_1(x) (((x) >> 8) & 0xff)

/* ------------------------------------------------------------------ */
/* Register Definitions				 								  */
/* ------------------------------------------------------------------ */

/* Register addresses */
#define ADT7420_TEMP_REG	0x00
#define ADT7420_STAT_REG    0x02
#define ADT7420_CFG_REG		0x03
#define ADT7420_THIGH_REG   0x04
#define ADT7420_TLOW_REG    0x06
#define ADT7420_TCRIT_REG   0x08
#define ADT7420_THYST_REG   0x0A
#define ADT7420_ID_REG      0x0B
#define ADT7420_RESET_REG   0x2F

/* Status Bits */
#define ADT7420_STAT_TLOW       (1 << 4)
#define ADT7420_STAT_THIGH      (1 << 5)
#define ADT7420_STAT_TCRIT      (1 << 6)
#define ADT7420_STAT_RDY        (1 << 7)

/* Fault Tolerance Bits */
#define ADT7420_CFG_FT_1        (0 << 0)    /* 1 fault */
#define ADT7420_CFG_FT_2        (1 << 0)    /* 2 fault */
#define ADT7420_CFG_FT_3        (2 << 0)    /* 3 fault */
#define ADT7420_CFG_FT_4        (3 << 0)    /* 4 fault */

/* Operation Mode Bits */
#define ADT7420_CFG_OP_CONT     (0 << 5)    /* Continuous conversion */
#define ADT7420_CFG_OP_1S       (1 << 5)    /* One shot */
#define ADT7420_CFG_OP_1SPS     (2 << 5)    /* One shot per second */
#define ADT7420_CFG_OP_SD       (3 << 5)    /* Shutdown */

/* Other Configuration Bits */
#define ADT7420_CFG_CT          (1 << 2)    /* CT pin polarity; 0 = active low */
#define ADT7420_CFG_INT         (1 << 3)    /* INT pin polarity; 0 = active low */
#define ADT7420_CFG_MODE        (1 << 4)    /* INT/CT mode; 0 = interrupt, 1 = comparator */
#define ADT7420_CFG_RES         (1 << 7)    /* Resolution; 0 = 13bit, 1 = 16bit */


/* ------------------------------------------------------------------ */
/* Sensor's methods 				 								  */
/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_adt7420_set_thresholds(sensor_t *sensor,
	unsigned char mask, unsigned char *thresholds);
static char sensor_adt7420_set_hysteresis(sensor_t *sensor,
	unsigned char *hysteresis);
static char sensor_adt7420_fill_rd(sensor_t *sensor, /* Function called by the core to get the sensor value */ 
	unsigned char *msg);
static char sensor_adt7420_init(sensor_t *sensor); /* Function called by the core to initialize the device */

/* ADT7420 temperature sensor methods */
sensor_methods_t PROGMEM sensor_adt7420_methods = {
    fill_event:		&sensor_threshold_fill_event,		/* Called when the core asks for event */ 
    fill_reading:	&sensor_adt7420_fill_rd,			/* Called when the core asks for sensor value */ 
    rearm:			&sensor_threshold_rearm,			/* Called when the core asks for sensor arm */ 
    set_thresholds:	&sensor_adt7420_set_thresholds,	    /* Called when a new threshold value is forced */ 
    get_thresholds:	&sensor_threshold_get_thresholds,	/* Called when thresholds value are requested */ 
    set_hysteresis:	&sensor_adt7420_set_hysteresis,	    /* Called to set a new threshold hysteresis value */ 
    get_hysteresis:	&sensor_threshold_get_hysteresis,	/* Called to get the threshold hysteresis value */ 
    init:			&sensor_adt7420_init				/* Called when the core asks for sensor init */ 
}; 

/* ------------------------------------------------------------------ */
/* Memory allocation 												  */
/* ------------------------------------------------------------------ */

/* Read-only info structures of ADT7420 temperature sensors */
static const sensor_adt7420_ro_t PROGMEM sensor_adt7420_ro[] = { 
	CFG_SENSOR_ADT7420 /* Defined in impc-config/config_sensors.h */ 
}; 

#define SENSOR_ADT7420_COUNT	sizeofarray(sensor_adt7420_ro) 

/* Read-write info structures of ADT7420 temperature sensors */
static struct sensor_adt7420 {
    sensor_threshold_t	sensor;
} sensor_adt7420[SENSOR_ADT7420_COUNT] WARM_BSS;

typedef struct sensor_adt7420 sensor_adt7420_t;

/* ------------------------------------------------------------------ */
/* Sensor declaration												  */
/* ------------------------------------------------------------------ */

static unsigned short sensor_adt7420_first;
DECL_SENSOR_GROUP(master, sensor_adt7420_ro, sensor_adt7420, &sensor_adt7420_first);

/* ------------------------------------------------------------------ */
/* Flag variable and state											  */
/* ------------------------------------------------------------------ */

static unsigned char sensor_adt7420_global_flags;
#define ADT7420_GLOBAL_INIT	    (1 << 0)	/* initialize all sensors */
#define ADT7420_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */
/* ADT7420 Register Read/Write Functions                			  */
/* ------------------------------------------------------------------ */

/* Write the ADT7420 configuration register */
static char inline sensor_adt7420_write_conf(unsigned short addr,
	unsigned char conf)
{
    return i2c_dev_write_reg(addr, ADT7420_CFG_REG, &conf, 1);
}

/* Write a ADT7420 temperature register (temp, Thigh, Tlow, Tcrit) 
   Input temp in integer degrees C.
*/
static char inline sensor_adt7420_write_temp(unsigned short addr,
	unsigned char reg, short temp)
{
    temp = temp << 7; // bit 7 = 1 C
    unsigned char data[2] = { GET_BYTE_1(temp), GET_BYTE_0(temp) };

    return i2c_dev_write_reg(addr, reg, data, 2);
}

/* Write ADT7420 Thyst register */
static char inline sensor_adt7420_write_hyst(unsigned short addr,
	unsigned char temp)
{
    return i2c_dev_write_reg(addr, ADT7420_THYST_REG, &temp, 1);
}

/* Read a ADT7420 temperature register (temp, Thigh, Tlow, Tcrit) 
   Returns temp in integer degrees C.
*/
static char inline sensor_adt7420_read_temp(unsigned short addr,
	unsigned char reg, unsigned char *temp)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, reg, data, 2)) {
        /* the sensor does not respond: set unreal temperature */
#ifdef DEBUG
        debug_printf("adt7420 @0x%04x, bad temperature reading\n", addr); 
#endif
        *temp = -99;
        return -1;
    }

    *temp = (char)( ((data[0] << 8) | data[1]) >> 7); // bit 7 = 1 C, returning temp in integer degrees C.

#ifdef DEBUG
    debug_printf("adt7420 @0x%04x, Temp reg 0x%02x: raw = 0x%02x%02x, temp = %d(0x%02x)\n", 
            addr, reg, data[0], data[1], *temp, *temp);
#endif

    return 0;
}

/* Read ADT7420 THYST register */
static char inline sensor_adt7420_read_thyst(unsigned short addr,
	unsigned char *thyst)
{
    if (i2c_dev_read_reg(addr, ADT7420_THYST_REG, thyst, 1)) {
        /* the sensor does not respond: set unreal thyst */
#ifdef DEBUG
        debug_printf("adt7420 @0x%04x, bad thyst reading\n", addr); 
#endif
        *thyst = -1;
        return -1;
    }

#ifdef DEBUG
    debug_printf("adt7420 @0x%04x, THyst: raw=0x%02x, THyst=%d\n",
            addr, *thyst, (*thyst) & 0xF
    );
#endif

    return 0;
}



/* Read ADT7420 status register */
static char inline sensor_adt7420_read_status(unsigned short addr,
	unsigned char *status)
{
    if (i2c_dev_read_reg(addr, ADT7420_STAT_REG, status, 1)) {
        /* the sensor does not respond: set unreal status (lower 4 bits should be 0) */
#ifdef DEBUG
        debug_printf("adt7420 @0x%04x, bad status reading\n", addr); 
#endif
        *status = 1;
        return -1;
    }

#ifdef DEBUG
    debug_printf("adt7420 @0x%04x, Status: raw=0x%02x, TLow=%d, THigh=%d, TCrit=%d\n", 
            addr, *status, 
            (*status & ADT7420_STAT_TLOW)  != 0, 
            (*status & ADT7420_STAT_THIGH) != 0,
            (*status & ADT7420_STAT_TCRIT) != 0
    );
#endif

    return 0;
}

/* Read ADT7420 configuration register */
static char inline sensor_adt7420_read_conf(unsigned short addr,
	unsigned char *conf)
{
    if (i2c_dev_read_reg(addr, ADT7420_CFG_REG, conf, 1)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("adt7420 @0x%04x, bad configuration reading\n", addr); 
#endif
        return -1;
    }

#ifdef DEBUG
    debug_printf("adt7420 @0x%04x, Config: raw=0x%02x, faults=%d, CT=%d, INT=%d, mode=%d, op=0x%02x, res=%d\n", 
            addr, *conf, 
            (*conf & 0x03) + 1, 
            (*conf & ADT7420_CFG_CT) != 0,
            (*conf & ADT7420_CFG_INT) != 0,
            (*conf & ADT7420_CFG_MODE) != 0,
            (*conf >> 5) & 0x03,
            (*conf & ADT7420_CFG_RES) != 0
    );
#endif

    return 0;
}

/* Read ADT7420 ID register */
static char inline sensor_adt7420_read_id(unsigned short addr,
	unsigned char *id)
{
    if (i2c_dev_read_reg(addr, ADT7420_ID_REG, id, 1)) {
        /* the sensor does not respond */
        return -1;
    }

#ifdef DEBUG
    debug_printf("adt7420 @0x%04x, ID: raw=0x%02x, revID=%d, manfID=%d\n", 
            addr, *id, (*id) & 0x7, ((*id) >> 3) & 0x1F
    );
#endif
    return 0;
}

/* ------------------------------------------------------------------ */
/* ADT7420 Specific Helper Functions                       			  */
/* ------------------------------------------------------------------ */

/* The following function updates the ADT7420 temperature limits */
static void sensor_adt7420_update_limits(unsigned short i2c_addr)
{
    /*
    unsigned short thigh, tlow, tcrit;
    unsigned char thyst;
    
    if (monly_i2c_is_ready(i2c_addr)) {

        thigh = 64;
        tlow = 10;
        tcrit = 147;
        thyst = 5;

#ifdef DEBUG
        debug_printf("adt7420 #%02x, thigh = %02x, tlow = %02x, tcrit = %02x, thyst = %01x \n", 
            num, thigh, thlow, tcrit, thyst
        );
#endif

        sensor_adt7420_write_temp(i2c_addr, ADT7420_THIGH_REG, thigh);
        sensor_adt7420_write_temp(i2c_addr, ADT7420_TLOW_REG, tlow);
        sensor_adt7420_write_temp(i2c_addr, ADT7420_TCRIT_REG, tcrit);
        sensor_adt7420_write_hyst(i2c_addr, thyst);
    }
    */
}

/* The following function initializes the given ADT7420 sensor. */
unsigned char initialize_sensor_adt7420(unsigned short i2c_addr)
{ 
    if (monly_i2c_is_ready(i2c_addr)) {
#ifdef DEBUG
        debug_printf("adt7420 #%02x, initialize\n", i2c_addr);
#endif

        /* Configure ADT7420:
        *  - continuous, interrupt mode
        *  - active low
        *  - one fault to generate event
        *  - 13-bit resolution
        */
        sensor_adt7420_write_conf(i2c_addr, ADT7420_CFG_OP_CONT |
            ADT7420_CFG_FT_1);

        /* update ADT7420 limits */
        sensor_adt7420_update_limits(i2c_addr);

        return 0;
    }
    return -1;
} 

/* The following function updates the reading of the given ADT7420 sensor. */
unsigned char read_sensor_adt7420(unsigned short i2c_addr)
{ 
    unsigned char reading;
    if (monly_i2c_is_ready(i2c_addr)) {

        /* read temperature */
        if (!sensor_adt7420_read_temp(i2c_addr, ADT7420_TEMP_REG, &reading)) {
            return reading;
        }
    }
    return -99;
} 

/* ------------------------------------------------------------------ */ 
/* This section contains sensor methods to be used externally	      */ 
/* ------------------------------------------------------------------ */ 

/* Sensor initialization. */
static char sensor_adt7420_init(sensor_t *sensor)
{
    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_adt7420_t *) sensor) - sensor_adt7420; 
     
    /* Execute init function */ 
    initialize_sensor_adt7420(sensor_adt7420_ro[i].i2c_addr); 

    return 0;
}

/* Set sensor thresholds */
static char sensor_adt7420_set_thresholds(sensor_t *sensor,
	unsigned char mask, unsigned char *thresholds)
{
    /* set thesholds */
    sensor_threshold_set_thresholds(sensor, mask, thresholds);

    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_adt7420_t *) sensor) - sensor_adt7420; 

    /* update limits */
    sensor_adt7420_update_limits(sensor_adt7420_ro[i].i2c_addr);

    return 0;
}

/* Set sensor hysteresis */
static char sensor_adt7420_set_hysteresis(sensor_t *sensor,
	unsigned char *hysteresis)
{
    /* set hysteresis */
    sensor_threshold_set_hysteresis(sensor, hysteresis);

    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_adt7420_t *) sensor) - sensor_adt7420; 

    /* update limits */
    sensor_adt7420_update_limits(sensor_adt7420_ro[i].i2c_addr);

    return 0;
}

/* Fill the Get Sensor Reading reply */
static char sensor_adt7420_fill_rd(sensor_t *sensor, unsigned char *msg)
{

    /* Get instance index using the pointer address */ 
    unsigned char i, sval; 
    unsigned short snum; 
     
    i = ((sensor_adt7420_t *) sensor) - sensor_adt7420; 
    sval = read_sensor_adt7420(sensor_adt7420_ro[i].i2c_addr); 
    snum = i + sensor_adt7420_first; 
     
    /* Update sensor value */ 
    sensor_threshold_update(&master_sensor_set, snum, sval, 0); 

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

/* ------------------------------------------------------------------ */
/* This section contains callbacks used to manage the sensor. 		  */
/* ------------------------------------------------------------------ */
#ifdef DEBUG
    unsigned char count = 0;
#endif

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_adt7420_1s_callback)
{
    unsigned char flags; 
 
#ifdef DEBUG
    unsigned short addr = 0x0292;
    unsigned char reading;
    debug_printf("rixu: enter TIMER_CALLBACK 1s\n");
    sensor_adt7420_read_temp(addr, ADT7420_TEMP_REG, &reading);
    count++;
    if (count == 10)
    {
        debug_printf("rixu: enter TIMER_CALLBACK 10s\n");
        sensor_adt7420_read_temp(addr, ADT7420_TLOW_REG, &reading);
        sensor_adt7420_read_temp(addr, ADT7420_THIGH_REG, &reading);
        sensor_adt7420_read_temp(addr, ADT7420_TCRIT_REG, &reading);
        sensor_adt7420_read_thyst(addr, &reading);
        sensor_adt7420_read_status(addr, &reading);
        sensor_adt7420_read_conf(addr, &reading);
        sensor_adt7420_read_id(addr, &reading);
        count = 0;
    }
#endif

    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_adt7420_global_flags |= ADT7420_GLOBAL_UPDATE; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 

/* Initialization callback */
INIT_CALLBACK(sensor_adt7420_init_callback)
{
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_adt7420_global_flags |= ADT7420_GLOBAL_INIT; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_adt7420_poll)
{
    unsigned char i, flags, gflags, pcheck, sval;
    unsigned short snum;


    /* Disable interrupts */ 
    save_flags_cli(flags); 
 
    /* Saved flag state into a local variable */ 
    gflags = sensor_adt7420_global_flags; 
 
    /* Clear flags */ 
    sensor_adt7420_global_flags = 0; 

    /* Enable interrupts */ 
    restore_flags(flags); 

    if (gflags & ADT7420_GLOBAL_INIT) {

        /* initialize all ADT7420 */
        for (i = 0; i < SENSOR_ADT7420_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
            pcheck = sensor_adt7420[i].sensor.s.status; 
 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                initialize_sensor_adt7420(sensor_adt7420_ro[i].i2c_addr); 
            } 
		} 
    }

    if (gflags & ADT7420_GLOBAL_UPDATE) { 
    	
        /* update all sensor readings */ 
        for (i = 0; i < SENSOR_ADT7420_COUNT; i++) { 

            /* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
        	pcheck = sensor_adt7420[i].sensor.s.status; 
        	snum = sensor_adt7420_first + i; 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                WDT_RESET; 
                sval = read_sensor_adt7420(sensor_adt7420_ro[i].i2c_addr); 
                sensor_threshold_update(&master_sensor_set, snum, sval, flags); 
            } 

        }

    }
}


#endif /* CFG_SENSOR_ADT7420 */
