#define NEED_MASTERONLY_I2C		/* Check that the Sensor_i2c bus is implemented */ 
 
#include <defs.h> 
#include <cfgint.h> 
#include <debug.h>
#include <stdbool.h>
 
#ifdef CFG_SENSOR_LTC2945			/* Compile the source only if at least one SENSOR_LTC2945 is implemented by the user */ 
 
#include <hal/i2c.h>				/* I2C functions */ 
#include <i2c_dev.h>				/* Master Only I2C functions */ 
#include <hal/system.h>				/* System functions */ 
	 
#include <app.h>					/* App functions */ 
#include <log.h>					/* Log functions */ 
#include <sensor.h>					/* Sensors functions */ 
#include <sensor_discrete.h>		/* Discrete sensor functions */ 
#include <sensor_threshold.h>		/* Threshold related functions */ 
 
#include <sensor_ltc2945.h>		/* Sensor related header file */ 
 
#ifndef HAS_MASTERONLY_I2C 
    #error Enable master-only I2C support to use LTC2945 sensors. 
#endif 
 
 
/* ------------------------------------------------------------------ */ 
/* Sensor's methods 				 								  */ 
/* ------------------------------------------------------------------ */ 
 
/* Sensor specific methods */ 
static char sensor_ltc2945_init(sensor_t *sensor);								/* Function called by the core to initialize the device */ 
 
sensor_methods_t PROGMEM sensor_ltc2945_methods = { 
    fill_event:		&sensor_threshold_fill_event,		/* Called when the core asks for event */ 
    fill_reading:	&sensor_ltc2945_fill_rd,			/* Called when the core asks for sensor value */ 
    rearm:			&sensor_threshold_rearm,			/* Called when the core asks for sensor arm */ 
    set_thresholds:	&sensor_threshold_set_thresholds,	/* Called when a new threshold value is forced */ 
    get_thresholds:	&sensor_threshold_get_thresholds,	/* Called when thresholds value are requested */ 
    set_hysteresis:	&sensor_threshold_set_hysteresis,	/* Called to set a new threshold hysteresis value */ 
    get_hysteresis:	&sensor_threshold_get_hysteresis,	/* Called to get the threshold hysteresis value */ 
    init:			&sensor_ltc2945_init				/* Called when the core asks for sensor init */ 
}; 
 
/* ------------------------------------------------------------------ */ 
/* Memory allocation 												  */ 
/* ------------------------------------------------------------------ */ 
static const sensor_ltc2945_ro_t PROGMEM sensor_ltc2945_ro[] = { 
	CFG_SENSOR_LTC2945 /* Defined in impc-config/config_sensors.h */ 
}; 
 
#define SENSOR_LTC2945_COUNT	sizeofarray(sensor_ltc2945_ro) 
 
/* Read-write info structures of LTC2945 temperature sensors */ 
static struct sensor_ltc2945 { 
    sensor_threshold_t	sensor; 
} sensor_ltc2945[SENSOR_LTC2945_COUNT] WARM_BSS; 
 
typedef struct sensor_ltc2945 sensor_ltc2945_t; 
   
 
/* ------------------------------------------------------------------ */ 
/* Sensor declaration												  */ 
/* ------------------------------------------------------------------ */ 
static unsigned short sensor_ltc2945_first; 
DECL_SENSOR_GROUP(master, sensor_ltc2945_ro, sensor_ltc2945, &sensor_ltc2945_first); 
 
/* ------------------------------------------------------------------ */ 
/* Flag variable and state											  */ 
/* ------------------------------------------------------------------ */ 
static unsigned char sensor_ltc2945_global_flags; 
 
#define LTC2945_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */ 
#define LTC2945_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */ 
 
/* ------------------------------------------------------------------ */ 
/* Register definitions                                 			  */ 
/* ------------------------------------------------------------------ */ 

#define LTC2945_CTRL_REG    0x00
#define LTC2945_ALERT_REG   0x01
#define LTC2945_STAT_REG    0x02
#define LTC2945_FAULT_REG   0x04 // clear on read register (0x03 is R/W)
#define LTC2945_POWER_REG   0x05
#define LTC2945_DSENS_REG   0x14
#define LTC2945_VIN_REG     0x1E
#define LTC2945_ADIN_REG    0x28

#define LTC2945_CTRL_SNAP       (1 << 7) // enable snapshot mode
#define LTC2945_CTRL_SNAP_DS    0x00 // measure delta sense in snapshot mode
#define LTC2945_CTRL_SNAP_AD    (1 << 6) // measure ADIN in snapshot mode
#define LTC2945_CTRL_SNAP_VIN   (1 << 5) // measure VIN in snapshot mode
#define LTC2945_CTRL_TEST       (1 << 4) // enable test mode
#define LTC2945_CTRL_BUSY       (1 << 3) // 1 = busy in snapshot mode (read-only)
#define LTC2945_CTRL_VIN        (1 << 2) // 1 = monitor SENSE+ (default), 0 = VDD
#define LTC2945_CTRL_SD         (1 << 1) // enable shutdown mode
#define LTC2945_CTRL_MULT       (1 << 0) // select the multiplier for power: 1 = VIN (default), 0 = ADIN

/* ------------------------------------------------------------------ */
/* Register Read/Write Functions                         			  */
/* ------------------------------------------------------------------ */

static char inline sensor_ltc2945_write_ctrl(unsigned short addr,
	unsigned char ctrl)
{
    return i2c_dev_write_reg(addr, LTC2945_CTRL_REG, &ctrl, 1);
}

static char inline sensor_ltc2945_read_ctrl(unsigned short addr, unsigned char *ctrl)
{
    if (i2c_dev_read_reg(addr, LTC2945_CTRL_REG, ctrl, 1)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("ltc2945 @0x%04x, bad control reading\n", addr); 
#endif
        return -1;
    }

#ifdef DEBUG
    debug_printf("ltc2945 @0x%04x CONTROL: raw = 0x%02x\n", addr, *ctrl);
#endif

    return 0;
}

static char inline sensor_ltc2945_read_stat(unsigned short addr, unsigned char *stat)
{
    if (i2c_dev_read_reg(addr, LTC2945_STAT_REG, stat, 1)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("ltc2945 @0x%04x, bad status reading\n", addr); 
#endif
        return -1;
    }

#ifdef DEBUG
    debug_printf("ltc2945 @0x%04x STATUS: raw = 0x%02x\n", addr, *stat);
#endif

    return 0;
}

// Read power. Returns result in watts. The power is the product of DELTASENSE and 
// VIN/ADIN. Need sense resistance (mO) to get power in watts. Note that the resolution of the power
// register depends on which register is chosen in control bit A0. Set adin=true if A0 = 0;
static char inline sensor_ltc2945_read_power(unsigned short addr, unsigned short rsns, bool adin, float *power)
{
    unsigned char data[3];

    if (i2c_dev_read_reg(addr, LTC2945_POWER_REG, data, 3)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("ltc2945 @0x%04x, bad power reading\n", addr); 
#endif
        *power = -1;
        return -1;
    }

    unsigned int val = (data[0] << 16) | (data[1] << 8) | data[2]; // units of 25uV * (adin) ? 0.5mV : 25mV
    *power = (float)(val) * 25 * ((adin) ? 0.5 : 25) / rsns; // units of uV * mV / mO = uW
    *power = *power / 1000000;

#ifdef DEBUG
    debug_printf("ltc2945 @0x%04x POWER: raw = 0x%02x%02x%02x (%d), power = %fmW\n", 
            addr, data[0], data[1], data[2], val, *vin);
#endif

    return 0;
}

// Read DELTASENSE. Returns value in millivolts.
static char inline sensor_ltc2945_read_dsens(unsigned short addr, float *dsens)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, LTC2945_DSENS_REG, data, 2)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("ltc2945 @0x%04x, bad delta sense reading\n", addr); 
#endif
        *dsens = -100;
        return -1;
    }

    unsigned short val = (unsigned short)((data[0] << 8) | data[1]) >> 4; // voltage in units of 25uV
    *dsens = (float)(val) * 0.025;

#ifdef DEBUG
    debug_printf("ltc2945 @0x%04x DELTASENSE: raw = 0x%02x%02x, delta = %fmV (%d * 25uV)\n", 
            addr, data[0], data[1], *dsens, val);
#endif

    return 0;
}

// Read VIN (either SENSE+ or VDD depending on CONTROL register). Returns
// value in volts.
static char inline sensor_ltc2945_read_vin(unsigned short addr, float *vin)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, LTC2945_VIN_REG, data, 2)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("ltc2945 @0x%04x, bad vin reading\n", addr); 
#endif
        *vin = -100;
        return -1;
    }

    unsigned short val = (unsigned short)((data[0] << 8) | data[1]) >> 4; // voltage in units of 25mV
    *vin = (float)(val) * 0.025;

#ifdef DEBUG
    debug_printf("ltc2945 @0x%04x VIN: raw = 0x%02x%02x, vin = %fV (%d * 25mV)\n", 
            addr, data[0], data[1], *vin, val);
#endif

    return 0;
}

// Read ADIN. Returns value in millivolts.
static char inline sensor_ltc2945_read_adin(unsigned short addr, float *adin)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, LTC2945_ADIN_REG, data, 2)) {
        /* the sensor does not respond */
#ifdef DEBUG
        debug_printf("ltc2945 @0x%04x, bad adin reading\n", addr); 
#endif
        *adin = -100;
        return -1;
    }

    unsigned short val = (unsigned short)((data[0] << 8) | data[1]) >> 4; // voltage in units of 0.5mV
    *adin = (float)(val) * 0.5;

#ifdef DEBUG
    debug_printf("ltc2945 @0x%04x ADIN: raw = 0x%02x%02x, adin = %fmV\n", 
            addr, data[0], data[1], *adin);
#endif

    return 0;
}

/* ------------------------------------------------------------------ */ 
/* This section contains functions specific to the device.			  */ 
/* ------------------------------------------------------------------ */ 
unsigned char initialize_sensor_ltc2945(unsigned short i2c_addr, unsigned short rsns){ 
    // default
    return 0x00; 
} 
 
// returns result in watts
unsigned char read_sensor_ltc2945(unsigned short i2c_addr, unsigned short rsns){ 
    float power;
    if (monly_i2c_is_ready(i2c_addr)) {
        if (!sensor_ltc2945_read_power(i2c_addr, rsns, false, &power)) {
            if (power < 0 || power >= 256)
                debug_printf("ltc2945 @0x%04x, power %f can not fit into an unsigned char\n", i2c_addr, power); 
            return (unsigned char)power;
        }
    }
    return -1;
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains Template sensor methods. 					  */ 
/* ------------------------------------------------------------------ */ 
 
/* Fill the Get Sensor Reading reply */ 
static char sensor_ltc2945_fill_rd(sensor_t *sensor, unsigned char *msg){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i, sval; 
    unsigned short snum; 
     
    i = ((sensor_ltc2945_t *) sensor) - sensor_ltc2945; 
    sval = read_sensor_ltc2945(sensor_ltc2945_ro[i].i2c_addr, sensor_ltc2945_ro[i].rsns); 
    snum = i + sensor_ltc2945_first; 
     
    /* Update sensor value */ 
    sensor_threshold_update(&master_sensor_set, snum, sval, 0); 
     
    return sensor_threshold_fill_reading(sensor, msg); 
 
} 
 
/* Sensor initialization. */ 
static char sensor_ltc2945_init(sensor_t *sensor){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_ltc2945_t *) sensor) - sensor_ltc2945; 
     
    /* Execute init function */ 
    initialize_sensor_ltc2945(sensor_ltc2945_ro[i].i2c_addr, sensor_ltc2945_ro[i].rsns); 
     
    return 0;    
 
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains callbacks used to manage the sensor. 		  */ 
/* ------------------------------------------------------------------ */ 
 
/* 1 second callback */ 
TIMER_CALLBACK(1s, sensor_ltc2945_1s_callback){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_ltc2945_global_flags |= LTC2945_GLOBAL_UPDATE; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Initialization callback */ 
INIT_CALLBACK(sensor_ltc2945_init_all){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_ltc2945_global_flags |= LTC2945_GLOBAL_INIT; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Main loop callback */ 
MAIN_LOOP_CALLBACK(sensor_ltc2945_poll){ 
 
    unsigned char i, flags, gflags, pcheck, sval; 
    unsigned short snum; 
 
    /* Disable interrupts */ 
    save_flags_cli(flags); 
 
    /* Saved flag state into a local variable */ 
    gflags = sensor_ltc2945_global_flags; 
 
    /* Clear flags */ 
    sensor_ltc2945_global_flags = 0; 
 
    /* Enable interrupts */ 
    restore_flags(flags); 
 
    if (gflags & LTC2945_GLOBAL_INIT) { 
 
        /* initialize all Template sensors */ 
        for (i = 0; i < SENSOR_LTC2945_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
            pcheck = sensor_ltc2945[i].sensor.s.status; 
 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                initialize_sensor_ltc2945(sensor_ltc2945_ro[i].i2c_addr, sensor_ltc2945_ro[i].rsns); 
            } 
		} 
    } 
 
    if (gflags & LTC2945_GLOBAL_UPDATE) { 
 
    	/* update all sensor readings */ 
        for (i = 0; i < SENSOR_LTC2945_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
        	pcheck = sensor_ltc2945[i].sensor.s.status; 
        	snum = sensor_ltc2945_first + i; 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                WDT_RESET; 
                sval = read_sensor_ltc2945(sensor_ltc2945_ro[i].i2c_addr, sensor_ltc2945_ro[i].rsns); 
                sensor_threshold_update(&master_sensor_set, snum, sval, flags); 
            } 
        } 
    } 
 
} 
 
#endif /* CFG_SENSOR_LTC2945 */ 