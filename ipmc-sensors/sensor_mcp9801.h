/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_mcp9801.c module.

    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_MCP9801_H__
#define __MASTER_SENSOR_MCP9801_H__

#include <sensor.h>

/* Read-only info structure of an MCP9801 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned short	i2c_addr;	/* I2C address of the chip */
} sensor_mcp9801_ro_t;

/* MCP9801 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_mcp9801_methods;

/* Auxiliary macro for defining MCP9801 sensor info */
#define SENSOR_MCP9801(s, a, alert)		\
    {						\
		SA(sensor_mcp9801_methods, s, alert),	\
		i2c_addr: (a) \
    }

#endif /* __MASTER_SENSOR_MCP9801_H__ */
