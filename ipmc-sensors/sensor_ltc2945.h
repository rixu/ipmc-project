/* 
    Description:
    This header file defines the interfaces
    provided by the sensor_ltc2945.c module.

    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_LTC2945_H__
#define __MASTER_SENSOR_LTC2945_H__

#include <sensor.h>

/* Read-only info structure of an Template sensor */
typedef struct {
    sensor_ro_t s;
    unsigned short i2c_addr;
    unsigned short rsns;
} sensor_ltc2945_ro_t;

/* Template sensor methods */
extern sensor_methods_t PROGMEM sensor_ltc2945_methods;

static char sensor_ltc2945_fill_rd(sensor_t *sensor, unsigned char *msg);

/* Auxiliary macro for defining Template sensor info */
#define SENSOR_LTC2945(s, i2c_addr_p, rsns_p, alert)		\
    {						\
        SA(sensor_ltc2945_methods, s, alert), \
        i2c_addr: (i2c_addr_p), \
        rsns: (rsns_p) \
    }
     
#endif /* __MASTER_SENSOR_LTC2945_H__ */
