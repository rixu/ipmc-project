/*
    IPMC sensor driver for the ADT7420 temperature sensor.
    
    Author: Riley Xu
    Created: 2019/06/10
*/

#ifndef __MASTER_SENSOR_ADT7420_H__
#define __MASTER_SENSOR_ADT7420_H__

#include <sensor.h>

/* Read-only info structure of a ADT7420 temperature sensor */
typedef struct {
    sensor_ro_t s;
    unsigned short	i2c_addr;	/* I2C address of the chip */
} sensor_adt7420_ro_t;

/* ADT7420 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_adt7420_methods;

/* Auxiliary macro for defining ADT7420 sensor info */
#define SENSOR_ADT7420(s, i2c_addr_p, alert)	\
    {						                    \
        SA(sensor_adt7420_methods, s, alert),   \
        i2c_addr: (i2c_addr_p)                  \
    }

#endif /* __MASTER_SENSOR_ADT7420_H__ */
