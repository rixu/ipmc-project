#define NEED_MASTERONLY_I2C		/* Check that the Sensor_i2c bus is implemented */

#include <defs.h>
#include <cfgint.h>
#include <debug.h>

#ifdef CFG_SENSOR_TEMPLATE			/* Compile the source only if at least one SENSOR_TEMPLATE is implemented by the user */

#include <hal/i2c.h>				/* I2C functions */
#include <i2c_dev.h>				/* Master Only I2C functions */
#include <hal/system.h>				/* System functions */
	
#include <app.h>					/* App functions */
#include <log.h>					/* Log functions */
#include <sensor.h>					/* Sensors functions */
#include <sensor_discrete.h>		/* Discrete sensor functions */
#include <sensor_threshold.h>		/* Threshold related functions */

#include <sensor_template.h>		/* Sensor related header file */

#ifndef HAS_MASTERONLY_I2C
    #error Enable master-only I2C support to use TEMPLATE sensors.
#endif


/* ------------------------------------------------------------------ */
/* Sensor's methods 				 								  */
/* ------------------------------------------------------------------ */

/* Sensor specific methods */
static char sensor_template_init(sensor_t *sensor);								/* Function called by the core to initialize the device */
static char sensor_template_fill_reading(sensor_t *sensor, unsigned char *msg);	/* Function called by the core to get the sensor value */ 

sensor_methods_t PROGMEM sensor_template_methods = {
    fill_event:		&sensor_threshold_fill_event,		/* Called when the core asks for event */
    fill_reading:	&sensor_template_fill_rd,			/* Called when the core asks for sensor value */
    rearm:			&sensor_threshold_rearm,			/* Called when the core asks for sensor arm */
    set_thresholds:	&sensor_threshold_set_thresholds,	/* Called when a new threshold value is forced */
    get_thresholds:	&sensor_threshold_get_thresholds,	/* Called when thresholds value are requested */
    set_hysteresis:	&sensor_threshold_set_hysteresis,	/* Called to set a new threshold hysteresis value */
    get_hysteresis:	&sensor_threshold_get_hysteresis,	/* Called to get the threshold hysteresis value */
    init:			&sensor_template_init				/* Called when the core asks for sensor init */
};

/* ------------------------------------------------------------------ */
/* Memory allocation 												  */
/* ------------------------------------------------------------------ */
static const sensor_template_ro_t PROGMEM sensor_template_ro[] = {
	CFG_SENSOR_TEMPLATE /* Defined in impc-config/config_sensors.h */
};

#define SENSOR_TEMPLATE_COUNT	sizeofarray(sensor_template_ro)

/* Read-write info structures of MCP9801 temperature sensors */
static struct sensor_template {
    sensor_threshold_t	sensor;
} sensor_template[SENSOR_TEMPLATE_COUNT] WARM_BSS;

typedef struct sensor_template sensor_template_t;
  

/* ------------------------------------------------------------------ */
/* Sensor declaration												  */
/* ------------------------------------------------------------------ */
static unsigned short sensor_template_first;
DECL_SENSOR_GROUP(master, sensor_template_ro, sensor_template, &sensor_template_first);

/* ------------------------------------------------------------------ */
/* Flag variable and state											  */
/* ------------------------------------------------------------------ */
static unsigned char sensor_template_global_flags;

#define TEMPLATE_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */
#define TEMPLATE_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */
/* This section contains functions specific to the device.			  */
/* ------------------------------------------------------------------ */
unsigned char initialize_sensor_template(unsigned short i2cAddr){
    //Init function (to be filled by the user)
    return 0x00;
}

unsigned char read_sensor_template(unsigned short i2cAddr){

	/* Example below set a signal, read its value and send an i2c command */

	unsigned short regptr = 0x00;
	unsigned char i2crep[1];

	/*
		Read command (2byte registers address):
			- i2cAddr: device I2C address
			- regptr: 16bit register ptr
			- i2crep: data byte array (read value)
			- 1: number of byte to be read

		For 1 byte register address, equivalent function exists:
			i2c_dev_read_reg(addrptr, regptr, &val, 1) (where regptr is an unsigned char)
	*/
	i2c_dev_read_2bytesReg(i2cAddr, regptr, i2crep, 1);

	return i2crep[0];
}

/* ------------------------------------------------------------------ */
/* This section contains Template sensor methods. 					  */
/* ------------------------------------------------------------------ */

/* Fill the Get Sensor Reading reply */
static char sensor_template_fill_rd(sensor_t *sensor, unsigned char *msg){

    /* Get instance index using the pointer address */
    unsigned char i, sval;
    unsigned short snum;
    
    i = ((sensor_template_t *) sensor) - sensor_template;
    sval = read_sensor_template(sensor_template_ro[i].i2c_addr);
    snum = i + sensor_template_first;
    
    /* Update sensor value */
    sensor_threshold_update(&master_sensor_set, snum, sval, 0);
    
    return sensor_threshold_fill_reading(sensor, msg);

}

/* Sensor initialization. */
static char sensor_template_init(sensor_t *sensor){

    /* Get instance index using the pointer address */
    unsigned char i = ((sensor_template_t *) sensor) - sensor_template;
    
    /* Execute init function */
    initialize_sensor_template(sensor_template_ro[i].i2c_addr);
    
    return 0;   

}

/* ------------------------------------------------------------------ */
/* This section contains callbacks used to manage the sensor. 		  */
/* ------------------------------------------------------------------ */

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_template_1s_callback){
    unsigned char flags;

    /*
     * -> Save interrupt state and disable interrupts
     *   Note: that ensure flags variable is not written by
     *         two processes at the same time.
     */
    save_flags_cli(flags);

    /* Change flag to schedule and update */
    sensor_template_global_flags |= TEMPLATE_GLOBAL_UPDATE;

    /*
     * -> Restore interrupt state and enable interrupts
     *   Note: restore the system
     */
    restore_flags(flags);
}

/* Initialization callback */
INIT_CALLBACK(sensor_template_init_all){
    unsigned char flags;

    /*
     * -> Save interrupt state and disable interrupts
     *   Note: that ensure flags variable is not written by
     *         two processes at the same time.
     */
    save_flags_cli(flags);

    /* Change flag to schedule and update */
    sensor_template_global_flags |= TEMPLATE_GLOBAL_UPDATE;

    /*
     * -> Restore interrupt state and enable interrupts
     *   Note: restore the system
     */
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_template_poll){

    unsigned char i, flags, gflags, pcheck, sval;
    unsigned short snum;

    /* Disable interrupts */
    save_flags_cli(flags);

    /* Saved flag state into a local variable */
    gflags = sensor_template_global_flags;

    /* Clear flags */
    sensor_template_global_flags = 0;

    /* Enable interrupts */
    restore_flags(flags);

    if (gflags & TEMPLATE_GLOBAL_INIT) {

        /* initialize all Template sensors */
        for (i = 0; i < SENSOR_TEMPLATE_COUNT; i++) {

        	/* Check if the sensor is present         */
        	/*    e.g.: can be absent in case of RTM  */
            pcheck = sensor_template[i].sensor.s.status;

            if (!(pcheck & STATUS_NOT_PRESENT)) {
                initialize_sensor_template(sensor_template_ro[i].i2c_addr);
            }
		}
    }

    if (gflags & TEMPLATE_GLOBAL_UPDATE) {

    	/* update all sensor readings */
        for (i = 0; i < SENSOR_TEMPLATE_COUNT; i++) {

        	/* Check if the sensor is present         */
        	/*    e.g.: can be absent in case of RTM  */
        	pcheck = sensor_template[i].sensor.s.status;
        	snum = sensor_template_first + i;
            if (!(pcheck & STATUS_NOT_PRESENT)) {
                sval = read_sensor_template(sensor_template_ro[i].i2c_addr);
                sensor_threshold_update(&master_sensor_set, snum, sval, flags);
            }
        }
    }

}

#endif /* CFG_SENSOR_MCP9801 */
